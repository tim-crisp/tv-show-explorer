# TV Show Explorer

A TV show explorer built with React using the [TVMAZE](http://www.tvmaze.com) api


# Getting started

1. `yarn`
2. `yarn start`

# Things to note
- PropTypes were only used in certain places simply to save time. Usually every components prop types would be present to improve documentation.
- Integration tests using a library such as [cypress](https://www.cypress.io/) would be written to ensure the application is functioning as intended in a browser environment.
- Unit tests would be written for all components and functionality to provide developer confidence. I haven't written any unit tests to save time, but please have a look in [the unit-test-examples](/unit-test-examples) to view some tests written for other projects.
- Error handling is partially implemented. In a real-world application all scenarios would be handled, but to save time, we assume that the API is always online.
- We'd usually dockerise the application

# Architectural Decisions (Desired)


NB: This design was too complex for the API being consumed. Had the API gave us the flexibility we needed, this would've been the best approach.


- Try to keep Shows/Episodes/Actors as independant as possible so they can be split into individual search components in the future (i.e. search by actor, search by episode), even though the API in use doesn't cater for this currently

If the ability to search by episode/actor was available, you'd abstract out the entire search system so you could add search capabilities with ease.

```js
function searchCapability({
  reducerId,
  queryUrl,
}) {
  // create actions, reducer, sagas, etc
  return { sagas, actions, reducer }
}
```

# Entities

- Character
  - Person
  - Show
- Person
  - Characters
  - Shows
- Show
  - Characters
  - Episodes
- Episode
  - Show


# Actions

- `SHOW_LOAD_CHARACTERS`
  - showId - the id of the show to load people for

- `SHOW_SEARCH` - dispatch with query (* indicates explore)
- `SHOW_SELECT`
  - `id` - id of the show being selected

- `ACTOR_LOAD`
  - `id` - id of the actor

- `EPISODE_LOAD_EPISODE`
  - `id` - id of the episode to load
- `EPISODE_LOAD_EPISODES`
  - `id` - ids of the episodes to load

# Components

- `ShowItem` - Represents a single TV show
- `ShowList` - The list of TV shows
- `ShowSearch` - The wrapper around searching (pagination, etc) defaults to * on mount


# Reducers

## Base

All of the 'Entities' will inherit the following structure for simplicity
```js
{
  cache: Number,                    // number to cache before purging
  <entitys>: {                      // the object containing the entities
    <entity_id>: {
      isLoading: Boolean,           // is the current entity loading
      hasError: Boolean,            // has the current entity failed to load?
      errorMessage: String          // what was the error?
    }
  }
}
```

## Show
```js
{
  cache: Number,          // shows to cache before purging
  results: [...showIds]     // list of shows obtained from the API
  currentPage: Number     // current page
  nextPage: Number        // next page
  totalResults: Number    // total number of results in result set
  selectedShow: String    // id of the selected show
  shows: {
    <show_id>: {
      ...showInfo,
      characters: []      // array of character ids
    }
  }
}
```

## Episode

```js
{
  cache: Number,            // episodes to cache before purging
  episodes: {
    <episode_id>: {
    }
  }
}
```

## Character

```js
{
  total: Number             // total number of characters loaded
  characters: {
    <character_id>: {
      personId: String,       // id of person who plays the character
      showId: String,         // id of the show the character appears in
    }
  }
}
```

## Person

```js
{
  total: Number             // total number of people loaded
  persons: {
    <person_id>: {
      characters: [],         // array of character ids
      shows: [],              // id of the show the character appears in
    }
  }
}
```
