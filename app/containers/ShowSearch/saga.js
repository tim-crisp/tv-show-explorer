import { call, put, takeLatest, delay } from 'redux-saga/effects';
import { isEmpty, complement } from 'ramda';
import request from 'utils/request';

import { showsLoaded, showsError, SHOWS_SEARCH } from './actions';

const notEmpty = complement(isEmpty);

export function* getShows({ query, debounce }) {
  if (debounce) {
    yield delay(1000);
  }

  let requestURL = 'http://api.tvmaze.com/shows';
  if (notEmpty(query)) {
    requestURL = `http://api.tvmaze.com/search/shows?q=${query}`;
  }

  try {
    // throw new Error('oh jesus!');
    const shows = yield call(request, requestURL);
    yield put(showsLoaded(shows));
  } catch (err) {
    yield put(showsError(err.message));
  }
}

// Individual exports for testing
export default function* showSearchSaga() {
  // See example in containers/HomePage/saga.js
  yield takeLatest(SHOWS_SEARCH, getShows);
}
