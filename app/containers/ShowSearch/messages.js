/*
 * ShowSearch Messages
 *
 * This contains all the text for the ShowSearch container.
 */

import { defineMessages } from 'react-intl';

export const scope = 'app.containers.ShowSearch';

export default defineMessages({
  header: {
    id: `${scope}.header`,
    defaultMessage: 'This is the ShowSearch container!',
  },
});
