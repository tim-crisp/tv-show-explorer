import { useEffect, useCallback } from 'react';
import { useSelector, useDispatch } from 'react-redux';

import {
  makeSelectShowsLoading,
  makeSelectShowsResults,
  makeSelectShowsOffset,
  makeSelectShowsOffsetIncrement,
} from './selectors';
import { showsSearch, showsLoad } from './actions';

const selectShowsResults = makeSelectShowsResults();
const selectShowsLoading = makeSelectShowsLoading();
const selectShowsOffset = makeSelectShowsOffset();
const selectShowsOffsetIncrement = makeSelectShowsOffsetIncrement();

export default function WithShows() {
  const dispatch = useDispatch();

  const offset = useSelector(selectShowsOffset);
  const offsetIncrement = useSelector(selectShowsOffsetIncrement);
  const searchResults = useSelector(selectShowsResults).slice(0, offset);
  const loading = useSelector(selectShowsLoading);

  useEffect(() => {
    if (!searchResults.length) {
      dispatch(showsSearch());
    }
  }, []);

  const loadMore = useCallback(
    visible => {
      if (!visible) return;
      dispatch(showsLoad(offset + offsetIncrement));
    },
    [offset, offsetIncrement, dispatch],
  );

  return {
    searchResults,
    loading,
    offset,
    offsetIncrement,
    loadMore,
  };
}
