import { createSelector } from 'reselect';
import { initialState } from './reducer';

const selectShowSearchDomain = state => state.show || initialState;

export const makeSelectShows = () =>
  createSelector(
    selectShowSearchDomain,
    substate => substate,
  );

export const makeSelectShowsQuery = () =>
  createSelector(
    selectShowSearchDomain,
    ({ query }) => query,
  );

export const makeSelectShowsLoading = () =>
  createSelector(
    selectShowSearchDomain,
    ({ loading }) => loading,
  );

export const makeSelectShowsError = () =>
  createSelector(
    selectShowSearchDomain,
    ({ error }) => error,
  );

export const makeSelectShowsResults = () =>
  createSelector(
    selectShowSearchDomain,
    ({ results }) => results,
  );

export const makeSelectShowsOffset = () =>
  createSelector(
    selectShowSearchDomain,
    ({ currentOffset }) => currentOffset,
  );

export const makeSelectShowsOffsetIncrement = () =>
  createSelector(
    selectShowSearchDomain,
    ({ offsetIncrement }) => offsetIncrement,
  );

export const makeSelectShowsHasError = () =>
  createSelector(
    selectShowSearchDomain,
    ({ hasError }) => !!hasError,
  );

export const makeSelectShowsErrorMessage = () =>
  createSelector(
    selectShowSearchDomain,
    ({ errorMessage }) => errorMessage,
  );

export default makeSelectShows;
