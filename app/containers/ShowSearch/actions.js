export const DEFAULT_ACTION = 'app/ShowSearch/DEFAULT_ACTION';
export const SHOWS_SEARCH = 'app/ShowSearch/SHOWS_SEARCH';
export const SHOWS_LOADED = 'app/ShowSearch/SHOWS_LOADED';
export const SHOWS_ERROR = 'app/ShowSearch/SHOWS_ERROR';
export const SHOWS_LOAD = 'app/ShowSearch/SHOWS_LOAD';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function showsSearch(query, debounce = true) {
  return {
    type: SHOWS_SEARCH,
    query: query || '',
    debounce,
  };
}

export function showsLoaded(results) {
  return {
    type: SHOWS_LOADED,
    results,
  };
}

export function showsError(message) {
  return {
    type: SHOWS_ERROR,
    message,
  };
}

export function showsLoad(offset) {
  return {
    type: SHOWS_LOAD,
    offset,
  };
}
