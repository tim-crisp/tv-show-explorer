/**
 *
 * ShowSearch
 *
 */

import React, { Fragment } from 'react';
import styled from 'styled-components';
import VisibilitySensor from 'react-visibility-sensor';
import { useInjectSaga } from 'utils/injectSaga';
import { useInjectReducer } from 'utils/injectReducer';
import reducer from './reducer';
import saga from './saga';
import withShows from './with-shows';
import Show from '../../components/Show';

const ShowWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
`;

export function ShowSearch() {
  useInjectReducer({ key: 'show', reducer });
  useInjectSaga({ key: 'show', saga });

  const { searchResults, loading, loadMore } = withShows();

  return (
    <Fragment>
      {loading || searchResults.length === 0 ? (
        'Loading...'
      ) : (
        <ShowWrapper>
          {searchResults.map(item => (
            <Show key={item.id} show={item} />
          ))}
          <VisibilitySensor onChange={loadMore}>
            <div style={{ display: 'absolute', width: '1px' }} />
          </VisibilitySensor>
        </ShowWrapper>
      )}
    </Fragment>
  );
}

export default ShowSearch;
