import produce from 'immer';
import { when, has, prop, map } from 'ramda';
import { SHOWS_SEARCH, SHOWS_LOADED, SHOWS_ERROR, SHOWS_LOAD } from './actions';

export const initialState = {
  isLoading: true,
  results: [],
  currentOffset: 20,
  offsetIncrement: 20,
  hasNext: false,
  hasPrevious: false,
  totalItems: 0,
  query: '',
};

const showsSearch = (draft, { query }) => {
  draft.isLoading = true;
  draft.query = query;
};

const pickResults = map(when(has('show'), prop('show')));

const showsLoaded = (draft, { results }) => {
  draft.isLoading = false;
  draft.results = pickResults(results);
  draft.totalItems = results.length;
  draft.currentOffset = draft.offsetIncrement;
  draft.hasError = false;
  delete draft.errorMessage;
};

const showsError = (draft, { message }) => {
  draft.isLoading = false;
  draft.results = [];
  draft.totalItems = 0;
  draft.currentOffset = draft.offsetIncrement;
  draft.hasError = true;
  draft.errorMessage = message;
};

const showsLoad = (draft, { offset }) => {
  draft.isLoading = false;
  draft.currentOffset = offset;
  draft.hasError = false;
  draft.loadMore = offset - draft.results.length <= 0;
  delete draft.errorMessage;
};

const reducer = {
  [SHOWS_SEARCH]: showsSearch,
  [SHOWS_LOADED]: showsLoaded,
  [SHOWS_ERROR]: showsError,
  [SHOWS_LOAD]: showsLoad,
};

/* eslint-disable default-case, no-param-reassign */
const showSearchReducer = (state = initialState, action) =>
  produce(state, draft => {
    if (action.type in reducer) {
      reducer[action.type](draft, action);
    }
  });

export default showSearchReducer;
