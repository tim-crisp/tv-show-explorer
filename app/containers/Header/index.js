import React, { useMemo, useCallback } from 'react';
import { Switch, Route } from 'react-router-dom';
import PropTypes from 'prop-types';
import { useSelector, useDispatch } from 'react-redux';
import Toolbar from '@material-ui/core/Toolbar';
import MaterialAppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade } from '@material-ui/core/styles/colorManipulator';
import { makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

import { showsSearch } from 'containers/ShowSearch/actions';
import { makeSelectShowsQuery } from 'containers/ShowSearch/selectors';

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      width: 120,
      '&:focus': {
        width: 200,
      },
    },
  },
}));

const SearchComponent = ({ classes }) => {
  const dispatch = useDispatch();
  const selectShowsQuery = useMemo(makeSelectShowsQuery, []);
  const query = useSelector(selectShowsQuery);

  const handleChange = useCallback(
    ({ target: { value } }) => {
      dispatch(showsSearch(value));
    },
    [dispatch],
  );

  return (
    <div className={classes.search}>
      <div className={classes.searchIcon}>
        <SearchIcon />
      </div>
      <InputBase
        placeholder="Search…"
        classes={{
          root: classes.inputRoot,
          input: classes.inputInput,
        }}
        inputProps={{ 'aria-label': 'Search' }}
        value={query}
        onChange={handleChange}
      />
    </div>
  );
};

SearchComponent.propTypes = {
  classes: PropTypes.object,
};

export default function AppBar() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MaterialAppBar position="static">
        <Toolbar>
          <Typography className={classes.title} variant="h6" noWrap>
            <Switch>
              <Route exact path="/search" render={() => 'TV Show Search'} />
            </Switch>
          </Typography>
          <Switch>
            <Route
              exact
              path="/search"
              render={() => <SearchComponent classes={classes} />}
            />
          </Switch>
        </Toolbar>
      </MaterialAppBar>
    </div>
  );
}
