/**
 *
 * App.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 */

import React from 'react';
import { Switch, Route } from 'react-router-dom';

import ShowSearch from 'containers/ShowSearch/Loadable';
import NotFoundPage from 'containers/NotFoundPage/Loadable';
import Wrapper from 'components/Wrapper';
import Header from 'containers/Header';
import GlobalStyle from '../../global-styles';

export default function App() {
  return (
    <Wrapper maxWidth="sm">
      <Header />
      <Switch>
        <Route exact path="/search" component={ShowSearch} />
        <Route component={NotFoundPage} />
      </Switch>
      <GlobalStyle />
    </Wrapper>
  );
}
