import React from 'react';
import PropTypes from 'prop-types';
import Container from '@material-ui/core/Container';

const Wrapper = ({ children }) => (
  <Container maxWidth="md">{children}</Container>
);

Wrapper.propTypes = {
  children: PropTypes.node,
};

export default Wrapper;
