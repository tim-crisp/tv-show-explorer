import React, { useState, useCallback } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import Card from '@material-ui/core/Card';
import CardMedia from '@material-ui/core/CardMedia';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Grow from '@material-ui/core/Grow';
import Chip from '@material-ui/core/Chip';
import ArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import ArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

import Cast from 'components/Cast';
import {
  getTitle,
  getRating,
  getDescription,
  getGenres,
  getImage,
} from 'components/Show/selectors';

const ShowCover = styled(CardMedia)`
  object-fit: cover;
  width: 210px !important;
`;

const ShowItem = styled(Card)`
  margin: 10px;
  display: flex;
  flex: 1 0 40%;
  min-width: 800px;
`;

const ShowContainer = styled.div`
  margin: 20px;
`;

const GenreChip = styled(Chip)`
  margin-right: 10px;
`;

function Show({ show }) {
  const [expanded, setExpanded] = useState(false);

  const toggleExpanded = useCallback(() => {
    setExpanded(!expanded);
  }, [expanded]);

  const ArrowIcon = expanded ? ArrowUpIcon : ArrowDownIcon;

  return (
    <React.Fragment>
      <Grow in>
        <ShowItem>
          <ShowCover
            component="img"
            src={getImage(show)}
            title={getTitle(show)}
          />
          <ShowContainer>
            <Grid container alignItems="center">
              <Grid item xs>
                <Typography gutterBottom variant="h4">
                  {getTitle(show)}
                </Typography>
              </Grid>
              <Grid item>
                <Typography gutterBottom variant="h6">
                  Rating: {getRating(show)}
                </Typography>
              </Grid>
            </Grid>
            <Typography color="textSecondary" variant="body2">
              <span
                dangerouslySetInnerHTML={{ __html: getDescription(show) }}
              />
            </Typography>

            <Grid container alignItems="center">
              <Grid item xs>
                {getGenres(show).map(genre => (
                  <GenreChip key={genre} label={genre} />
                ))}
              </Grid>
              <Grid item>
                <IconButton
                  aria-label="Delete"
                  size="small"
                  onClick={toggleExpanded}
                >
                  <Typography color="textSecondary" variant="body2">
                    More Info
                  </Typography>
                  <ArrowIcon fontSize="inherit" />
                </IconButton>
              </Grid>
            </Grid>
            <Cast show={show} visible={expanded} />
          </ShowContainer>
        </ShowItem>
      </Grow>
    </React.Fragment>
  );
}

Show.propTypes = {
  show: PropTypes.object.isRequired,
};

export default Show;
