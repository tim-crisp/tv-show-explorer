import {
  __,
  path,
  ifElse,
  always,
  compose,
  isNil,
  isEmpty,
  either,
  concat,
} from 'ramda';

export const isNilOrEmpty = either(isNil, isEmpty);

export const getTitle = path(['name']);

export const getRating = compose(
  ifElse(
    isNilOrEmpty,
    always('Unavailable'),
    compose(
      concat(__, ' / 10'),
      String,
    ),
  ),
  path(['rating', 'average']),
);

export const getDescription = path(['summary']);

export const getGenres = path(['genres']);

export const getImage = path(['image', 'medium']);
