import React, { memo, useState, useMemo } from 'react';
import { path, uniq, compose, map } from 'ramda';
import styled from 'styled-components';

import Typography from '@material-ui/core/Typography';

import loadCast from './load';

const getName = path(['person', 'name']);

const Actor = styled.span`
  &:not(:last-child) {
    &:after {
      content: ', ';
    }
  }
`;

const getActors = compose(
  uniq,
  map(getName),
);

export default memo(function Cast({ show, visible }) {
  const [cast, setCast] = useState();

  useMemo(() => {
    if (!cast && visible) {
      loadCast(show.id).then(
        response => typeof setCast === 'function' && setCast(response),
      );
    }
  }, [cast, visible]);

  if (!visible) return null;

  if (!cast) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <Typography variant="h6">Cast</Typography>
      <div>
        <Typography color="textSecondary" variant="body2">
          {cast &&
            cast.length > 0 &&
            getActors(cast).map(actor => <Actor key={actor}>{actor}</Actor>)}
        </Typography>
      </div>
    </div>
  );
});
