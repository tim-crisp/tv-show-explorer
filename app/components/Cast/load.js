import request from 'utils/request';

export default function loadCast(showId) {
  return request(`http://api.tvmaze.com/shows/${showId}/cast`);
}
