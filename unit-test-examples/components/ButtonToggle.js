import React, { memo } from 'react';
import PropTypes from 'prop-types';
import R from 'ramda';
import Button from './Button';

const circularNth = R.curry((index, list) => R.nth(index % list.length, list));

const currentOption = R.ifElse(
  R.propSatisfies(R.isNil, 'initialValue'),
  R.prop('defaultOption'),
  R.prop('initialValue')
);

const getIndexOfNextOption = R.compose(
  R.add(1),
  R.converge(R.indexOf, [
    currentOption,
    R.prop('optionKeys'),
  ]),
);

const getNextOptionKey = R.compose(
  R.converge(circularNth, [
    getIndexOfNextOption,
    R.prop('optionKeys'),
  ])
);

const ButtonToggle = memo(({
  visible,
  options,
  initialValue,
  defaultOption,
  onChange,
  ...props,
}) => {
  if (!visible) return null;

  const optionKeys = R.keys(options);
  const nextOptionKey = getNextOptionKey({
    initialValue,
    defaultOption,
    optionKeys,
  });

  return (
    <Button
      {...props}
      onClick={() => onChange(props.name, nextOptionKey)}
      data-test="next-option-button"
    >
      <span data-test="next-option">{options[nextOptionKey]}</span>
    </Button>
  );
});

ButtonToggle.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  dataTest: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  text: PropTypes.string,
  style: PropTypes.object,
  visible: PropTypes.bool,
  disabled: PropTypes.bool,
  type: PropTypes.oneOf(['primary', 'secondary', 'tertiary', 'clear']),
  initialValue: PropTypes.any,
  defaultOption: PropTypes.any,
};

ButtonToggle.defaultProps = {
  visible: true,
  disabled: false,
  className: '',
};

export default ButtonToggle;
