import React from 'react';
import { shallow } from 'enzyme';
import test from 'tape';
import sinon from 'sinon';
import ButtonToggle from './ButtonToggle';

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const fakeProps = {
    visible: false,
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  assert.true(target.isEmptyRender(), 'renders null when not visible');
});

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const fakeProps = {
    options: {
      firstOption: 'First Option',
      secondOption: 'Second Option',
    },
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  const actual = target.find('[data-test="next-option"]').text();
  const expected = 'First Option';

  assert.equals(actual, expected, 'next option defaults to first option (second option selected) when no default option has been provided');
});

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const fakeProps = {
    options: {
      firstOption: 'First Option',
      secondOption: 'Second Option',
    },
    defaultOption: 'firstOption',
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  const actual = target.find('[data-test="next-option"]').text();
  const expected = 'Second Option';

  assert.equals(actual, expected, 'next option defaults to second option (first option selected) when the default selected option is the first option');
});

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const fakeProps = {
    options: {
      firstOption: 'First Option',
      secondOption: 'Second Option',
    },
    defaultOption: 'firstOption',
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  const actual = target.find('[data-test="next-option"]').text();
  const expected = 'Second Option';

  assert.equals(actual, expected, 'next option defaults to second option (first option selected) when the default selected option is the first option');
});

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const onChangeSpy = sinon.spy();
  const fakeProps = {
    name: 'Fake Name',
    options: {
      firstOption: 'First Option',
      secondOption: 'Second Option',
    },
    defaultOption: 'firstOption',
    onChange: onChangeSpy,
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  target.find('[data-test="next-option-button"]').simulate('click');

  const actual = onChangeSpy.args[0];
  const expected = ['Fake Name', 'secondOption'];

  assert.deepEquals(actual, expected, 'on change is called with second option when first option is the default option with no selected option provided');
});

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const onChangeSpy = sinon.spy();
  const fakeProps = {
    name: 'Fake Name',
    options: {
      firstOption: 'First Option',
      secondOption: 'Second Option',
    },
    defaultOption: 'firstOption',
    initialValue: 'secondOption',
    onChange: onChangeSpy,
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  target.find('[data-test="next-option-button"]').simulate('click');

  const actual = target.find('[data-test="next-option"]').text();
  const expected = 'First Option';

  assert.equals(actual, expected, 'next option is set to first option when second option selected');
});

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const onChangeSpy = sinon.spy();
  const fakeProps = {
    name: 'Fake Name',
    options: {
      firstOption: 'First Option',
      secondOption: 'Second Option',
      thirdOption: 'Third Option',
    },
    defaultOption: 'firstOption',
    initialValue: 'secondOption',
    onChange: onChangeSpy,
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  target.find('[data-test="next-option-button"]').simulate('click');

  const actual = target.find('[data-test="next-option"]').text();
  const expected = 'Third Option';

  assert.equals(actual, expected, 'next option is set to third option when second option is selected');
});

test('<ButtonToggle/> component', assert => {
  assert.plan(1);
  const onChangeSpy = sinon.spy();
  const fakeProps = {
    name: 'Fake Name',
    options: {
      firstOption: 'First Option',
      secondOption: 'Second Option',
      thirdOption: 'Third Option',
    },
    defaultOption: 'firstOption',
    initialValue: 'thirdOption',
    onChange: onChangeSpy,
  };
  const target = shallow(<ButtonToggle {...fakeProps} />);

  target.find('[data-test="next-option-button"]').simulate('click');

  const actual = target.find('[data-test="next-option"]').text();
  const expected = 'First Option';

  assert.equals(actual, expected, 'next option is set to first option when third option is selected');
});
